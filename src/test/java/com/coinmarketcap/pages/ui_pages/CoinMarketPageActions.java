package com.coinmarketcap.pages.ui_pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class CoinMarketPageActions extends UIInteractionSteps {

    @Step
    public void clickShowRows() {
        find(CoinMarketLandingPage.SHOW_ROWS).click();
    }

    @Step
    public void showRowsSelected(String value1) {
        find(CoinMarketLandingPage.showDropDown(value1)).click();
        waitForCondition().until(
                        ExpectedConditions.visibilityOfElementLocated(CoinMarketLandingPage.
                                showDropDownSelected(value1)))
                .isSelected();
    }

    @Step
    public List<String> recordPriceDetails() {

        List<String> priceDetails = new ArrayList<>();
        List<WebElement> rows = find(CoinMarketLandingPage.COIN_MARKET_TABLE).
                findElements(By.xpath("//tr"));
        for (WebElement row : rows) {
            List<WebElement> cols = row.findElements(By.xpath("//td"));
            priceDetails.add(cols.get(3).getText());
        }
        return priceDetails;
    }


    @Step
    public List<String> storePositions() {
        List<String> coinPositions = new ArrayList<>();
        List<WebElement> rows = find(CoinMarketLandingPage.COIN_MARKET_TABLE).
                findElements(By.tagName("tr"));
        for (WebElement row : rows) {
            List<WebElement> cols = row.findElements(By.xpath("//td"));
            coinPositions.add(cols.get(2).getText());
        }
        return coinPositions;
    }

    @Step
    public List<Map> recordCoinDetails() {
        List<Map> coinDetails = new ArrayList<>();

        List<WebElement> rows = find(CoinMarketLandingPage.COIN_MARKET_TABLE).
                findElements(By.xpath("//tr"));

        for (WebElement row : rows.subList(1, rows.size())) {
            List<WebElement> cols = row.findElements(By.xpath("//td"));

            Map<String, String> coinDetail = new Hashtable<String, String>();
            coinDetail.put("coin_position", cols.get(1).getText());

            coinDetail.put("coin_name", cols.get(2).getText());
            coinDetail.put("coin_price", cols.get(3).getText());
            coinDetail.put("coin_market_cap", cols.get(7).getText());
            coinDetail.put("coin_volume", cols.get(8).getText());
            coinDetails.add(coinDetail);
        }

        return coinDetails;
    }

    @Step
    public void enterPriceValues(String fromValue, String toValue) {
//        find(By.xpath("//*[@id=\"__next\"]/div/div[1]/div[2]/div/div/ul/li[5]/button[contains(.,'1 More Filter')]")).click();
        find(By.xpath("//li[5]/button[contains(.,'Add Filter')]")).click();

        find(CoinMarketLandingPage.PRICE_BUTTON).click();

        find(CoinMarketLandingPage.PRICE_FROM).sendKeys(fromValue);
        find(CoinMarketLandingPage.PRICE_TO).sendKeys(toValue);


    }


    @Step
    public void clickApplyButton() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        WebElement element = find(CoinMarketLandingPage.PRICE_APPLY);
        js.executeScript("arguments[0].click();", element);
    }

    @Step
    public void clickShowResults() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        WebElement element = find(CoinMarketLandingPage.SHOW_RESULTS);
        js.executeScript("arguments[0].click();", element);
    }

    @Step
    public void clickCloseCookie() {
        find(CoinMarketLandingPage.CLOSE_COOKIES).click();

    }

    @Step
    public void clickCloseSomePopUp() {
        withTimeoutOf(Duration.ofSeconds(30)).find(CoinMarketLandingPage.CLOSE_SOME_POP_UP).click();
    }

    public void clickOnFilterButton(String value, String anotherValue) {

        find(By.xpath("//*[@id=\"__next\"]/div/div[1]/div[2]/div/div/div[4]/div[2]/div[3]/div[2]/button[contains(.,'Filters')]")).click();

        withTimeoutOf(Duration.ofSeconds(30))
                .find(By.xpath("//button[text()='" + value + "']")).click();


        withTimeoutOf(Duration.ofSeconds(30))
                .find(By.xpath("//li[text()='" + anotherValue + "']"))
                .click();


    }

    public void filterBy(String oneMoreFilter, String allCrytoType) {

        find(By.xpath("//button[contains(.,'" + oneMoreFilter + "')]")).click();

        find(By.xpath("//button[contains(.,'" + allCrytoType + "')]")).click();


    }

    public void turn(String toggleName, String state) {
//       if ( find(CoinMarketLandingPage.MINEABLE).getAttribute("class").contains("kZTSDM")){
        find(CoinMarketLandingPage.MINEABLE).click();
        //}

    }
}