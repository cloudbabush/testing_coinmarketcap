package com.coinmarketcap.stepdefinitions;

import com.coinmarketcap.pages.ui_pages.CoinMarketLandingPage;
import com.coinmarketcap.pages.ui_pages.CoinMarketPageActions;
import com.coinmarketcap.pages.ui_pages.FilterPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.ArrayList;
import java.util.List;

public class UISteps {

    @Steps
    CoinMarketLandingPage coinMarketLandingPage;

    @Steps
    CoinMarketPageActions coinMarketPageActions;

    @Steps
    FilterPage filterPage;

    List<String> allPriceDetails = new ArrayList<>();

    @Given("User launch Coin Market Page")
    public void user_launch_coin_market_page() {
        coinMarketLandingPage.open();

        coinMarketPageActions.clickCloseSomePopUp();
        coinMarketPageActions.clickCloseCookie();
    }

    @Given("record the number of records in the page")
    public void record_the_number_of_records_in_the_page() {
        allPriceDetails = coinMarketPageActions.recordPriceDetails();
    }


    @When("User filter the currencies by price between {} and {}")
    public void user_filter_the_currencies_by_price(String fromValue, String toValue) {
        coinMarketPageActions.enterPriceValues(fromValue, toValue);
        coinMarketPageActions.clickApplyButton();


    }

    @Then("Verify that the page should display the currencies with the price between {} and {}")
    public void verify_that_the_page_should_display_the_currencies_with_the_price(Double value1, Double value2) {


        List<String> filteredPriceDetails = coinMarketPageActions.recordPriceDetails();
        filterPage.verifyPriceRange(filteredPriceDetails, value1, value2);
    }


    @Given("User select Shows rows drop down to {} value")
    public void the_user_select_shows_rows_dropdown(String value1) {
        coinMarketPageActions.clickShowRows();
        coinMarketPageActions.showRowsSelected(value1);
    }

    @Given("User Verify that top {} crypto coins by market cap  are getting displayed on page")
    public void verify_that_rows_are_getting_displayed_on_page(Integer value) {
        filterPage.verifyNumberOfRows(value);
    }

    @Given("User clicks on filter {string}button and chooses {string}")
    public void user_clicks_on_filter_button_and_chooses(String value, String anotherValue) {
        coinMarketPageActions.clickOnFilterButton(value, anotherValue);

    }

    @And("capture all suitable information")
    public void captureAllSuitableInformation() {
        coinMarketPageActions.recordCoinDetails();
    }

    @And("User filters the {string} by  {string}")
    public void userFiltersTheBy(String oneMoreFilter, String allCrytoType) {
        coinMarketPageActions.filterBy(oneMoreFilter, allCrytoType);

    }


    @And("{string} is turned {string}")
    public void isTurned(String toggleName, String state) {
        coinMarketPageActions.turn(toggleName, state);
        coinMarketPageActions.clickShowResults();
    }
}
