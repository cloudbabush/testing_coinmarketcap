package com.coinmarketcap.stepdefinitions;

import com.coinmarketcap.pages.api_pages.ApiActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.*;

public class ApiSteps {

    @Steps
    ApiActions apiActions;
    private BigDecimal price;
    private Response response;


    @Given("User converts {} in {} having {} to {} having {}")
    public void userConvertsAmountInCurrencyFromHavingCurrencyFrom_symbolToCurrencyToHavingCurrencyTo_symbol(BigDecimal amount
            , String currencyFrom, String currencyFrom_symbol, String currencyTo, String currencyTo_symbol) {

        Response response = apiActions.getPriceConversion(amount, currencyFrom_symbol, currencyTo_symbol);
        price = response.path("data[0].quote." + currencyTo_symbol + ".price");

    }


    @And("user able to convert that {} to {string} having symbol {string}")
    public void userAbleToConvertThatCurrencyTo_symbolToHavingSymbol(String currencyFrom_symbol, String cryptoName, String crypto_symbol) {
        response = apiActions.getPriceConversion(price, currencyFrom_symbol, crypto_symbol);

    }

    @Then("verify that the response has the following details {string} and {}")
    public void verifyThatTheResponseHasTheFollowingDetailsAndCurrencyTo(String cryptoSymbol, String coinName) {
        response.then().assertThat().body("data.name", hasItem(coinName));
        response.then().assertThat().body("data.quote[0]", hasKey(cryptoSymbol));
        response.then().assertThat().body("data.quote." + cryptoSymbol + ".price", notNullValue());

    }

}
