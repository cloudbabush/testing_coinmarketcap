package com.coinmarketcap.pages.api_pages;

import com.coinmarketcap.utils.ReadConfigurationFile;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

import java.math.BigDecimal;

import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.path.json.config.JsonPathConfig.NumberReturnType.BIG_DECIMAL;

public class ApiActions {

    private Response response;


    public Response getPriceConversion(BigDecimal amount, String currencyFrom_symbol, String currencyTo_symbol) {

        response = SerenityRest.with().config(SerenityRest.config().jsonConfig(jsonConfig().numberReturnType(BIG_DECIMAL)))
                .given()
                .header("X-CMC_PRO_API_KEY", ReadConfigurationFile.
                        readConfigurationFile("api.key")).log().all()
                .queryParam("amount", amount)
                .queryParam("symbol", currencyFrom_symbol)
                .queryParam("convert", currencyTo_symbol)
                .header("Accept", "application/json")
                .when()
                .get(ReadConfigurationFile.
                        form_endpoint("coinmarket.priceconversion.endpoint"));

        response.then().log().all()
                .statusCode(200);
        return response;

    }


}