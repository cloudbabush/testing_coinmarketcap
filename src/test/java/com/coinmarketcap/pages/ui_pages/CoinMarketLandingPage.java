package com.coinmarketcap.pages.ui_pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("coinmarketcap.page")
public class CoinMarketLandingPage extends PageObject {

    public static By SHOW_ROWS = By.xpath("//div[contains(@class, 'sc-16r8icm-0 ekMmID table-control-page-sizer')]//div[contains(@class, 'sc-16r8icm-0 tu1guj-0 cSTqvK')]");

    public static By CLOSE_COOKIES = By.className("cmc-cookie-policy-banner__close");
    public static By CLOSE_SOME_POP_UP = By.className("gv-close");


    public static By COIN_MARKET_TABLE = By.xpath("//table/tbody");
    public static By PRICE_BUTTON = By.xpath("//button[contains(text(),'Price')]");
    public static By PRICE_FROM = By.xpath("//input[@placeholder='$0']");

    public static By MINEABLE = By.xpath("//*[@id='mineable']/span");


    public static By PRICE_TO = By.xpath("//input[@placeholder='$99,999']");
    public static By PRICE_APPLY = By.xpath("//button[contains(text(),'Apply')]");

    public static By SHOW_RESULTS = By.xpath("//button[contains(text(),'Show results')]");

    private static final String SHOW_ROW_DROP_DOWN = "//div[contains(@class, 'tippy-content')]//button[contains(text(), '%s')]";
    private static final String ROW_DOWN_SELECTED_VALUE = "//div[contains(@class, 'sc-16r8icm-0 ekMmID table-control-page-sizer')]//div[contains(text(), '%s')]";


    public static By showDropDown(String value) {
        return By.xpath(
                String.format(SHOW_ROW_DROP_DOWN, value)
        );
    }

    public static By showDropDownSelected(String value) {
        return By.xpath(
                String.format(ROW_DOWN_SELECTED_VALUE, value)
        );
    }
}
