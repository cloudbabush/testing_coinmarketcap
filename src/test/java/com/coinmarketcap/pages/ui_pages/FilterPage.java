package com.coinmarketcap.pages.ui_pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FilterPage extends UIInteractionSteps {

    @Step
    public void verifyNumberOfRows(int expectedCount) {
        int actualCount = find(CoinMarketLandingPage.COIN_MARKET_TABLE).
                findElements(By.tagName("tr")).size();
        assertThat(actualCount).isEqualTo(expectedCount);
    }

    @Step
    public void verifyPriceRange(List<String> filteredPriceDetails, Double fromPrice, Double toPrice) {
        List<BigDecimal> filteredPrices = new ArrayList<>();
        for (String filteredPriceDetail : filteredPriceDetails) {
            filteredPrices.add(new BigDecimal(filteredPriceDetail.substring(1).replaceAll(",", "")));
        }
        for (BigDecimal filteredPrice : filteredPrices) {
            assertThat(filteredPrice.compareTo(new BigDecimal(fromPrice)) >= 0 && filteredPrice.compareTo(new BigDecimal(toPrice)) <= 0).isTrue();
        }
    }


}
