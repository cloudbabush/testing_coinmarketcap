# Testing Coin Market Cap Project  using Serenity Cucumber Library

.

## Key Tools/Framework/Libraries

- Maven - build tool
- Serenity library
- Cucumber - BDD/Gherkin style feature files
- Serenity HTML Report

## Project Structure
```
.
├── README.md
├── pom.xml                   Dependency for libraries
├── serenity.properties       Endpoints for API's
├── src
│   ├── main
│   └── test                  Test runners and supporting code 
└── target
    ├── classes
    ├── failsafe-reports
    ├── generated-test-sources
    ├── maven-archiver
    ├── maven-status
    ├── serenity-rest-cucumber-1.0.0-SNAPSHOT.jar
    ├── site                  Serenity HTML Report inside target/site/serenity/index.html
    └── test-classes

```
### How to run the test on local machine
- Prerequisites: maven3+, java11+ 
- Junit :
    - go to src/test/java/ and run class CoinMarketCapTestRunner.java
      (will run all test scenarios with **@smoketest** tag by default)
    - You can also modify the tags you want to execute from **@CucumberOptions** inside the class and use @api or @ui 
- Maven :
    - run command from base project :

      ```terminal
      $ mvn clean verify
      ```

      (will run all scenarios with **@smoketest** tag by default)

    - Serenity HTML report is generated when running the mvn command mentioned above in target directory - open target/site/serenity/index.html after the execution.
      Alternatively you download the sample report folder contents to view a sample report