Feature: Frontend Task

  @ui  @ui_sc1 @smoketest
  Scenario: Show rows dropdown value
    Given User launch Coin Market Page
    When User select Shows rows drop down to 20 value
    Then User Verify that top 20 crypto coins by market cap  are getting displayed on page
    And capture all suitable information


  @ui @smoketest @ui_sc2
  Scenario:Filtering UI and extracting data for comparison
    Given User launch Coin Market Page
    And User select Shows rows drop down to 20 value
    Then  capture all suitable information
    And User clicks on filter "Algorithm"button and chooses "PoW"
    And User filter the currencies by price between 100 and 10000
    And User filters the "All Cryptocurrencies" by  "Coins"
    And  "Mineable" is turned "ON"
    Then Verify that the page should display the currencies with the price between 100 and 10000
    And capture all suitable information
