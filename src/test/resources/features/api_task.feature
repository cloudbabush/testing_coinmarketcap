Feature: Api Task

  @api  @api_sc1 @smoketest
  Scenario Outline: Price Conversion
    Given User converts <amount> in <currencyFrom> having <currencyFrom_symbol> to <currencyTo> having <currencyTo_symbol>
    And user able to convert that <currencyTo_symbol> to "Dogecoin" having symbol "DOGE"
    Then verify that the response has the following details "DOGE" and <currencyTo>

    Examples:
      | amount   | currencyFrom       | currencyFrom_symbol | currencyTo     | currencyTo_symbol |
      | 10000000  | Guatemalan Quetzal | GTQ                 | Pound Sterling | GBP               |
